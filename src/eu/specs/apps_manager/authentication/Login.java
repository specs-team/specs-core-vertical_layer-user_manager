package eu.specs.apps_manager.authentication;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Login extends HttpServlet{
	
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
	
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
				
		response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String role = request.getParameter("role");
        
        try {
            System.out.println("3)LOGIN");
             //Redirect to User/Developer/Owner page
        	System.out.println("Valid Login= user: "+username+", "
            		+ "password: "+password+", role: "+role);
        	if(role.compareTo("User")==0){
        		request.getRequestDispatcher( "/reserved/user.jsp" ).forward(request,response);
        		
        	}else if(role.compareTo("Developer")==0){
        		request.getRequestDispatcher( "/reserved/developer.jsp" ).forward(request,response);
        		
        	}else if(role.compareTo("Owner")==0){
        		request.getRequestDispatcher( "/reserved/owner.jsp" ).forward(request,response);
        	}else{
        		request.getRequestDispatcher( "/reserved/index.jsp" ).forward(request,response);
        		System.err.println("Problem with role name!");
        	}	
            
            //request.getRequestDispatcher( "/reserved/success.jsp" ).forward(request,response);
		} finally {            
            out.close();
        }
    }
}
