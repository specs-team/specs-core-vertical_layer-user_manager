package eu.specs.apps_manager.authentication;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class AuthenticationFilter implements Filter{

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		System.out.println("Authentication filter init");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("1)Authentication filter activated");
		
		String username = request.getParameter("username");
	    String password = request.getParameter("password");
	    String role = request.getParameter("role");
		Ldap ldap= new Ldap();
       
        
		try {
			if(ldap.validateLogin(username, password, role+"s")==true){
				System.out.println("Valid Authentication= user: "+username+", "
                		+ "password: "+password+", role: "+role);
				chain.doFilter(request, response);
			}else{
                request.getRequestDispatcher( "/errorPage.jsp" ).forward(request,response);
            	System.out.println("Invalid Authentication= user: "+username+", "
                		+ "password: "+password+", role: "+role);
            }
		} catch (NamingException e) {
			e.printStackTrace();
		}
		
	}

	
	
	@Override
	public void destroy() {
		
	}

}
