package eu.specs.apps_manager.authentication;

import java.io.IOException;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Signup extends HttpServlet{
	//This class implements the signup function
	private static final long serialVersionUID = 1L;

	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		
		response.setContentType("text/html;charset=UTF-8");
        
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String role = request.getParameter("role");
        
        try{
            System.out.println("SIGNUP");
            
            Ldap ldap= new Ldap();
            /*
            if(ldap.validateLogin(username, password, "owners")==false
            		&& ldap.validateLogin(username, password, "developers")==false
            		&& ldap.validateLogin(username, password, "users")==false){
            		*/
            	//Create new account
            	if(ldap.createUser(username, password, role+"s")==true){
            		request.getRequestDispatcher( "/successSignup.jsp" ).forward(request,response);
            	}else{
            		//Username already exists 
            		request.getRequestDispatcher( "/errorSignup.jsp" ).forward(request,response);
                	System.out.println("Invalid Signup= user: "+username+", "
                    		+ "password: "+password+", role: "+role);
            	}
            /*
            }
           else{
            	//Account already exists
            	request.getRequestDispatcher( "/errorSignup.jsp" ).forward(request,response);
            	System.out.println("Invalid Signup= user: "+username+", "
                		+ "password: "+password+", role: "+role);
            }
            */
        	
        } catch (NamingException e) {
        	e.printStackTrace();
		} finally {            
            
        }
	}
}
