package eu.specs.apps_manager.authentication;

import javax.naming.directory.*;
import javax.naming.*;
import java.util.Properties;

public class Ldap {
	
	public static DirContext  ldapContext = null;
	public static String contFact = "com.sun.jndi.ldap.LdapCtxFactory";
	public static String url ="ldap://localhost:389";
	public static String conn_type="simple";
	public static String dn = "cn=Manager,dc=specs,dc=eu"; //admin user
	public static String rootpwd= "pass"; //root password
	
//-------Connect to OpenLDAP server
	public void connect(String user,String pwd) throws NamingException{
		Properties prop = new Properties();
		prop.put(Context.INITIAL_CONTEXT_FACTORY,contFact);
		prop.put(Context.PROVIDER_URL,url);
		prop.put(Context.SECURITY_AUTHENTICATION,conn_type);
		prop.put(Context.SECURITY_PRINCIPAL,user);
		prop.put(Context.SECURITY_CREDENTIALS,pwd);
		
		try{
			ldapContext = new InitialDirContext(prop);
			System.out.println("Connected");
			
		}catch(NamingException ex){
			ex.printStackTrace();
			System.err.println("Connection Refused!!!");
	}
		
  }
	
	
//--------------Create user	----------
	public Boolean createUser(String name,String pwd,String role)throws NamingException{
		Properties prop = new Properties();
		prop.put(Context.INITIAL_CONTEXT_FACTORY,contFact);
		prop.put(Context.PROVIDER_URL,url);
		prop.put(Context.SECURITY_AUTHENTICATION,conn_type);
		prop.put(Context.SECURITY_PRINCIPAL,dn);
		prop.put(Context.SECURITY_CREDENTIALS,rootpwd);
		
		try{
			DirContext ctx = new InitialDirContext(prop);
		
			//Creation user attributes
			Attribute objClasses= new BasicAttribute("objectclass");
			objClasses.add("top");
			objClasses.add("person");
			objClasses.add("inetOrgPerson");
		
			Attribute cn= new BasicAttribute("cn",name);
			Attribute sn= new BasicAttribute("sn",name);
			Attribute uid= new BasicAttribute("uid",name);
			Attribute userPassword= new BasicAttribute("userPassword",pwd);
		
			Attributes attrs = new BasicAttributes();
			attrs.put(objClasses);
			attrs.put(cn);
			attrs.put(sn);
			attrs.put(uid);
			attrs.put(userPassword);
		
			//Creation user
			ctx.createSubcontext("cn="+name+",ou="+role+",dc=specs,dc=eu",attrs);
				
			System.out.println("Create new user: cn="+name+",ou="+role+",dc=specs,dc=eu");
			return true;
			}
		catch(NameAlreadyBoundException ex){
			//Exception activated if DN name already exists
			//ex.printStackTrace();
			System.err.println("DN already exists!");
			return false;
		}
	}
	
///---------Validate Login
		public Boolean validateLogin(String name,String pwd,String role) throws NamingException{
		Boolean auth=false;
		Properties prop = new Properties();
		prop.put(Context.INITIAL_CONTEXT_FACTORY,contFact);
		prop.put(Context.PROVIDER_URL,url);
		prop.put(Context.SECURITY_AUTHENTICATION,conn_type);
		prop.put(Context.SECURITY_PRINCIPAL,dn);
		prop.put(Context.SECURITY_CREDENTIALS,rootpwd);
		
		try{
			DirContext ctx = new InitialDirContext(prop);
			
			String[] attrs = new String[4];
			attrs[0]="cn";
			attrs[1]="sn";
			attrs[2]="uid";
			attrs[3]="userPassword";
			
			Attributes res = ctx.getAttributes("cn="+name+",ou="+role+",dc=specs,dc=eu",attrs);
			
			if (res==null){
				System.out.println("No Attributes found!");
			}else{
						Attribute attr= res.get("cn");
						if(attr != null){
							
							// User Exists, Validate the Password
							Properties prop2 = new Properties();
							prop2.put(Context.INITIAL_CONTEXT_FACTORY,contFact);
							prop2.put(Context.PROVIDER_URL,url);
							prop2.put(Context.SECURITY_AUTHENTICATION,conn_type);
				            prop2.put(Context.SECURITY_PRINCIPAL,"cn="+name+",ou="+role+",dc=specs,dc=eu");
				            prop2.put(Context.SECURITY_CREDENTIALS, pwd);
				            DirContext ctx2 = new InitialDirContext(prop2);
				            auth = true;
						}
						else auth = false;
						
					}
		} catch (AuthenticationException ex) { 
	    	// Invalid Login
	    	System.err.println("Invalid Login!");
	    	auth = false;
	    }catch (NameNotFoundException ex) { 
	    	// The base context was not found
	    	System.err.println("The base context was not found!");
	    	auth = false;
	    }catch(NamingException ex){
			ex.printStackTrace();
			auth = false;
		}
		return auth;
	}
		
//--------------Delete user	----------
		public void deleteUser(String name,String pwd,String role)throws NamingException{
			//TODO: coming soon.....to be tested
			Properties prop = new Properties();
			prop.put(Context.INITIAL_CONTEXT_FACTORY,contFact);
			prop.put(Context.PROVIDER_URL,url);
			prop.put(Context.SECURITY_AUTHENTICATION,conn_type);
			prop.put(Context.SECURITY_PRINCIPAL,dn);
			prop.put(Context.SECURITY_CREDENTIALS,rootpwd);
			
			try{
				DirContext ctx = new InitialDirContext(prop);
				
				String[] attrs = new String[4];
				attrs[0]="cn";
				attrs[1]="sn";
				attrs[2]="uid";
				//attrs[3]="userPassword";
				
				Attributes res = ctx.getAttributes("cn="+name+",ou="+role+",dc=specs,dc=eu",attrs);
				if (res==null){
					System.out.println("No Attributes found!");
				}else{
					// User Exists, Delete operation
					ctx.destroySubcontext("cn="+name+",ou="+role+",dc=specs,dc=eu");
										
				}
				
			}catch(NamingException e){
				e.printStackTrace();
			}
		}
		
//------------------------------getAttributes
		public void getAttrs(String name,String role)throws NamingException{
			Properties prop = new Properties();
			prop.put(Context.INITIAL_CONTEXT_FACTORY,contFact);
			prop.put(Context.PROVIDER_URL,url);
			prop.put(Context.SECURITY_AUTHENTICATION,conn_type);
			prop.put(Context.SECURITY_PRINCIPAL,dn);
			prop.put(Context.SECURITY_CREDENTIALS,rootpwd);
			
			try{
				DirContext ctx = new InitialDirContext(prop);
				
				String[] attrs = new String[4];
				attrs[0]="cn";
				attrs[1]="sn";
				attrs[2]="uid";
				attrs[3]="userPassword";
				
				Attributes res = ctx.getAttributes("cn="+name+",ou="+role+",dc=specs,dc=eu",attrs);
				
				if (res==null){
					System.out.println("No Attributes found!");
				}else{
					//print of all attributes
							Attribute attr= res.get("cn");
							if(attr != null){
								System.out.println("cn:");
								
								for (NamingEnumeration vals = attr.getAll(); vals.hasMoreElements(); 
										System.out.println("\t" + vals.nextElement()));
							}
							
							attr = res.get("sn");
						    if (attr != null){
						      System.out.println("sn:");
						      for (NamingEnumeration vals = attr.getAll(); vals.hasMoreElements();
						    		  System.out.println("\t" + vals.nextElement()));
						    }

						    attr = res.get("uid");
						    if (attr != null){
						      System.out.println("uid:");
						      for (NamingEnumeration vals = attr.getAll(); vals.hasMoreElements();
						    		  System.out.println("\t" + vals.nextElement()));
						    }

						    attr = res.get("userPassword");
						    if (attr != null){
						      System.out.println("userPassword:");
						      for (NamingEnumeration vals = attr.getAll(); vals.hasMoreElements();
						    		  System.out.println("\t" + vals.nextElement()));
						    }
					}
		}catch(NamingException ex){
				ex.printStackTrace();
		}
	}		 
		
	public static void main(String[] args) throws NamingException{
	    	Ldap ldap= new Ldap();
	    	//ldap.connect(dn,rootpwd);
	    	//ldap.createUser("pierluca", "pier", "users");
	    	//ldap.createUser("andrea", "andrea", "users");
	    	//ldap.createUser("casola", "casola", "owners");
	    	ldap.createUser("rak", "rak", "developers");
	    	
	    	//ldap.getAttrs("pierluca","users");
	    	//ldap.getAttrs("rak","developers");
	    	//ldap.connect("cn=rak,ou=developers,dc=specs,dc=eu", "rak");
	    	/*
	    	if(ldap.validateLogin("rak", "rak", "developers")==true) 
	    		System.out.println("OK, rak ha effettuato il login");
	    		
	    	else{
	    		System.out.println("Niente login per rak!");
	    	}
	    	*/
	    }
}


/*
 *  Legend
 * 	dn: Distingued Name
	uid: User id
	cn: Common Name
	sn: Surname
	l: Location
	ou: Organizational Unit
	o: Organization
	dc: Domain Component
	st: State
	c: Country
 */
