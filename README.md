User Manager

The User Manager component provides a shared access control mechanism to all SPECS applications. In particular, it integrates a common authentication mechanism based on LDAP (OpenLDAP) and a role-based authorization mechanism (XACML).

The component is implemented as a web application, the *SPECS Apps Manager*, which can be integrated by other SPECS Applications in order to support user management. The application uses an external authentication mechanism to manage accounts and a Repository policy to store the files containing the access control policies.
